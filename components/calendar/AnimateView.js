import React, { Component } from "react";
import * as Animatable from "react-native-animatable";
import Icon from "react-native-vector-icons/FontAwesome";

export default class AnimateView extends Component {
	handleTextRef = ref => (this.text = ref);
	bounce = () =>
		this.view
			.bounce(800)
			.then(endState =>
				console.log(
					endState.finished ? "bounce finished" : "bounce cancelled"
				)
			);

	render() {
		return (
			<Animatable.View ref={this.handleViewRef}>
				<Icon name="angle-double-down" color="black" size={30} />
			</Animatable.View>
		);
	}
}

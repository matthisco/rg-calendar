import React, { Component } from "react";
import { connect } from "react-redux";
import * as moment from "moment";
import * as helpers from "../../helpers";
import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    ActivityIndicator
} from "react-native";
import { CalendarList } from "react-native-calendars";
import CustomHeader from "../../navigation/CustomHeader";

import { fetchEvents, eventsForCalendarMonth } from "../../actions/events";
import { navigate } from "../../navigation/actions";
class Month extends Component {
    static navigationOptions = {
        title: "",
        header: <CustomHeader />
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.onDayPress = this.onDayPress.bind(this);
        this.loadItems = this.loadItems.bind(this);
    }

    componentDidMount() {
        const {
            credentials: { year, group, student },
            fetchEvents
        } = this.props;
        const Id = student || group;
        fetchEvents(Id);
    }

    onDayPress(day) {
        const {
            navigation: { navigate }
        } = this.props;
        navigate("Day", { day });
    }

    loadItems(day) {
        const { events, eventsForMonth } = this.props;
        eventsForMonth(events, day);
    }

    render() {
        const currentDate = helpers.currentDate;

        const {
            events,
            isLoading,
            hasErrored,
            navigation,
            dispatch
        } = this.props;

        if (isLoading) {
            return (
                <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size="large" color="#6da1a6" />
                </View>
            );
        }

        return (
            <CalendarList
                style={styles.calendar}
                current={currentDate}
                markingType={"multi-dot"}
                //      onVisibleMonthsChange={this.loadItems}
                onDayPress={this.onDayPress}
                minDate={currentDate}
                loadingIndicatorColor={"#6da1a6"}
                markedDates={events}
                pastScrollRange={0}
                futureScrollRange={6}
                scrollEnabled={true}
                showScrollIndicator={true}
            />
        );
    }
}

const mapStateToProps = state => {
    return {
        events: state.fetchEvents,
        hasErrored: state.hasErrored,
        isLoading: state.isLoading,
        credentials: state.setCredentials
    };
};

const mapDispatchToProps = dispatch => ({
    fetchEvents: id => dispatch(fetchEvents(id)),
    eventsForMonth: (events, day) =>
        dispatch(eventsForCalendarMonth(events, day))
});

export default connect(mapStateToProps, mapDispatchToProps)(Month);

const styles = StyleSheet.create({
    calendar: {
        paddingTop: 5,
        flex: 1
    },
    container: {
        flex: 1,
        justifyContent: "center"
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
});

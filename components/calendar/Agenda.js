import React, { Component } from "react";
import { connect } from "react-redux";
import { Text, View, StyleSheet } from "react-native";
import { Agenda } from "react-native-calendars";
import * as helpers from "../../helpers";
import { eventsForMonth, fetchEvents } from "../../actions/events";
import CustomHeader from "../../navigation/CustomHeader";
import Icon from "react-native-vector-icons/FontAwesome";

import * as Animatable from "react-native-animatable";

import AgendaItem from "./AgendaItem";

class AgendaScreen extends Component {
    static navigationOptions = {
        title: "",
        header: <CustomHeader />
    };

    constructor(props) {
        super(props);
        this.renderItem = this.renderItem.bind(this);
        this.renderEmptyDate = this.renderEmptyDate.bind(this);
        this.rowHasChanged = this.rowHasChanged.bind(this);
        this.loadItems = this.loadItems.bind(this);
    }

    componentDidMount() {
        const {
            eventsForYear,
            navigation: {
                state: {
                    params: { day }
                }
            }
        } = this.props;

        this.props.dispatch(eventsForMonth(eventsForYear, day));
    }

    render() {
        const {
            eventsMonth,
            eventsForYear,
            navigation: {
                state: {
                    params: { day }
                }
            }
        } = this.props;
        const currentDate = helpers.currentDate;

        return (
            <Agenda
                items={eventsMonth}
                loadItemsForMonth={this.loadItems}
                selected={day.dateString}
                markedDates={eventsForYear}
                renderKnob={this.renderKnob}
                renderItem={this.renderItem}
                renderEmptyDate={this.renderEmptyDate}
                rowHasChanged={this.rowHasChanged}
                markingType={"multi-dot"}
                minDate={currentDate}
            />
        );
    }

    renderKnob() {
        return <Icon name="angle-double-down" size={30} color="#006666" />;
    }

    loadItems(day) {
        const { eventsForYear } = this.props;
        this.props.dispatch(eventsForMonth(eventsForYear, day));
    }

    renderItem(item) {
        return (
            <View style={[styles.item]}>
                <AgendaItem item={item} />
            </View>
        );
    }

    renderEmptyDate() {
        return (
            <View style={styles.emptyDate}>
                <Text />
            </View>
        );
    }

    rowHasChanged(r1, r2) {
        return r1.name !== r2.name;
    }
}

const mapStateToProps = state => {
    return {
        eventsMonth: state.eventsForMonth,
        eventsForYear: state.fetchEvents
    };
};

export default connect(mapStateToProps)(AgendaScreen);

const styles = StyleSheet.create({
    item: {
        flex: 1,
        borderRadius: 2,
        marginRight: 10,
        marginTop: 17
    },
    emptyDate: {
        height: 15,
        flex: 1,
        paddingTop: 30
    }
});

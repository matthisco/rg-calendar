import React from "react";
import { Text, View, WebView } from "react-native";

const AgendaItem = ({
	item: {
		title,
		venue,
		colour,
		organiser,
		startTime,
		endTime,
		description,
		allDay,
		textColor
	}
}) => {
	return (
		<View
			style={{
				backgroundColor: colour || "#ffffff",
				padding: 10
			}}
		>
			<Text style={{ color: textColor }}>{title}</Text>
			<Text style={{ color: textColor }}>{venue}</Text>

			{organiser ? (
				<Text style={{ color: textColor }}>{organiser}</Text>
			) : null}

			{description ? (
				<WebView html={description} style={{ color: "textColor" }} />
			) : null}

			{startTime && endTime ? (
				<Text style={{ color: textColor }}>
					{startTime} - {endTime}
				</Text>
			) : null}
		</View>
	);
};

export default AgendaItem;

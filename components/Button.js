import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, Text, View } from 'react-native';



import * as theme from '../theme';

class Button extends Component {

  onPress = () => {
    if (!this.props.disabled) this.props.onPress();
  };

  render() {
    let buttonStyle = [styles.buttonStyle, this.props.buttonStyle];
    if (this.props.disabled) {
      const buttonStyleDisabled = [styles.buttonStyleDisabled, this.props.buttonStyleDisabled];
      buttonStyle = [...buttonStyle, ...buttonStyleDisabled];
    }
    const fullWidthStyle = this.props.fullWidth && { alignSelf: 'stretch' };

    return (
   
          <View style={buttonStyle}>
            <Text style={[styles.labelStyle, this.props.labelStyle]}>
              {this.props.label}
            </Text>
          </View>

    );
  }
}

Button.defaultProps = {
  buttonStyle: undefined,
  buttonStyleDisabled: undefined,
  disabled: false,
  fullWidth: false,
  labelStyle: undefined,
  style: undefined,
};

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  buttonStyle: PropTypes.any,
  buttonStyleDisabled: PropTypes.any,
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool,
  gradient: PropTypes.array,
  labelStyle: PropTypes.any,
  style: PropTypes.any,
};

export default Button;

const styles = StyleSheet.create({
  buttonStyle: {
    padding: theme.metrics.paddingVerticalButton,
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  buttonStyleDisabled: {
    opacity: theme.metrics.touchableOpacity,
  },
  labelStyle: {

    color: theme.colors.white,
  },
});

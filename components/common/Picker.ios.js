import React, { Component } from "react";

import {
  StyleSheet,
  ActionSheetIOS,
  View,
  Text,
  TouchableOpacity
} from "react-native";

const PickerList = props => {
  const { label, options, selectedValue, name, onChange, identifier } = props;

  const optionList = options.map(option => option.label);
  const selectedOption = options.find(option => option.id == selectedValue);

  const onSelectCategory = () => {
    ActionSheetIOS.showActionSheetWithOptions({ options: optionList }, function(
      option
    ) {
                       onChange(options[option].id);
    });
  };

  return (
    <TouchableOpacity onPress={onSelectCategory}>
      <Text style={styles.picker}>
        {selectedOption ? selectedOption.label : `Please choose a ${label}`}
      </Text>
    </TouchableOpacity>
  );
};

export default PickerList;

const styles = StyleSheet.create({
  picker: {
    padding: 10,
    width: 300
  }
});

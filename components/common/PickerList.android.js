import React, { Component } from "react";

import { StyleSheet, Picker } from "react-native";

const PickerList = props => {
  const { label, options, selectedValue, name, onChange, identifier } = props;
  return (
    <Picker
      selectedValue={selectedValue}
      onValueChange={onChange}
    >
      <Picker.Item
        key={"unselectable"}
        label={"Please select an option"}
        value=""
      />
      {options.map((option, i) => {
        return <Picker.Item key={option.id} label={option.label} value={option.id} />;
      })}
    </Picker>
  );
};

export default PickerList;

import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";

import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableHighlight,
    ActivityIndicator,
    ImageBackground,
    AsyncStorage,
    Dimensions
} from "react-native";

import {
    setYear,
    setStudent,
    setGroup,
    setCredentials,
    resetForm
} from "../actions/events";

import { isLoading, isLoadingCredentials, hasErrored } from "../actions/loader";

import { navigate } from "../navigation/actions";

import Picker from "./common/Picker";

class HomeScreen extends Component {
    static navigationOptions = {
        title: ""
    };

    constructor(props) {
        super(props);
        this.state = { hideElement: false };
        this.onLayout = this.onLayout.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onParentChange = this.onParentChange.bind(this);
        this.onGroupChange = this.onGroupChange.bind(this);
        this.onStudentChange = this.onStudentChange.bind(this);
    }

    componentWillMount() {
        const {
            navigation: { navigate },
            setCredentials,
            isLoadingCredentials
        } = this.props;

        isLoadingCredentials(true);
        AsyncStorage.getItem("loggedIn")
            .then(user => {
                if (user) {
                    setCredentials(JSON.parse(user));
                    navigate("Month");
                } else {
                    isLoadingCredentials(false);
                }
            })
            .catch(() => {
                isLoadingCredentials(false); // Error
            });
    }

    onParentChange(e) {
        this.props.resetForm();
        this.props.setYear(e);
    }

    onGroupChange(e) {
        if (e !== "") {
            const {
                credentials: { year },
                categories
            } = this.props;
            this.props.setGroup(e, categories, year);
        }
    }

    onStudentChange(e) {
        if (e !== "") {
            this.props.setStudent(e);
        }
    }

    onSubmit(e) {
        AsyncStorage.setItem(
            "loggedIn",
            JSON.stringify(this.props.credentials)
        );
        this.props.navigation.navigate("Month");
    }

    onLayout(e) {
        const { width, height } = Dimensions.get("window");

        if (width > height) {
            this.setState({ hideElement: true });
        } else {
            this.setState({ hideElement: false });
        }
    }

    render() {
        const {
            isLoading,
            categories,
            hasErrored,
            isLoadingCredentials,
            credentials: { year, group, student, showStudent }
        } = this.props;

        if (isLoading || hasErrored || categories == 0) {
            return (
                <ImageBackground
                    source={require("../img/bkgPhoto.jpg")}
                    style={{
                        flex: 1,
                        width: null,
                        height: null
                    }}
                >
                    <View
                        style={styles.container}
                        onLayout={this.onLayout.bind(this)}
                    >
                        <Image
                            source={require("../img/CalendarApp_logo.png")}
                            style={{ width: 200, height: 100 }}
                        />
                    </View>
                </ImageBackground>
            );
        }

        return (
            <ImageBackground
                source={require("../img/bkgPhoto.jpg")}
                style={{
                    flex: 1,
                    width: null,
                    height: null
                }}
            >
                <View
                    style={styles.container}
                    onLayout={this.onLayout.bind(this)}
                >
                    {!this.state.hideElement ? (
                        <View style={{ alignSelf: "flex-start" }}>
                            <Image
                                source={require("../img/RGUC_Connect_logo.png")}
                                style={{
                                    width: 100,
                                    height: 125
                                }}
                            />
                        </View>
                    ) : null}
                    <Image
                        source={require("../img/CalendarApp_logo.png")}
                        style={{ width: 200, height: 100 }}
                    />

                    <View style={{ backgroundColor: "white" }}>
                        <Picker
                            selectedValue={year}
                            label="Year"
                            onChange={this.onParentChange}
                            options={categories}
                            style={styles.picker}
                        />
                        {year ? (
                            <Picker
                                selectedValue={group}
                                label="Group"
                                onChange={this.onGroupChange}
                                options={
                                    categories.find(category => {
                                        return category.id == year;
                                    }).options
                                }
                            />
                        ) : null}
                        {year && group && showStudent ? (
                            <Picker
                                selectedValue={student}
                                label="Student"
                                onChange={this.onStudentChange}
                                options={
                                    categories
                                        .find(category => {
                                            return category.id == year;
                                        })
                                        .options.find(category => {
                                            return category.id == group;
                                        }).options
                                }
                            />
                        ) : null}
                        {(year && group && !showStudent) || student ? (
                            <TouchableHighlight
                                style={styles.button}
                                onPress={this.onSubmit}
                            >
                                <Text style={styles.buttonText}> Submit</Text>
                            </TouchableHighlight>
                        ) : null}
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return {
        categories: state.fetchCategories,
        isLoading: state.isLoading,
        hasErrored: state.hasErrored,
        credentials: state.setCredentials,
        isLoadingCredentials: state.isLoadingCredentials
    };
};

const mapDispatchToProps = dispatch => ({
    isLoadingCredentials: loadingCredentials =>
        dispatch(isLoadingCredentials(loadingCredentials)),
    setCredentials: credentials => dispatch(setCredentials(credentials)),
    setYear: year => dispatch(setYear(year)),
    setGroup: (group, categories, year) =>
        dispatch(setGroup(group, categories, year)),
    setStudent: student => dispatch(setStudent(student)),
    resetForm: () => dispatch(resetForm())
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
        padding: 30
    },
    button: {
        alignItems: "center",
        backgroundColor: "#800080",
        padding: 10
    },
    picker: {
        width: 300,
        height: 30
    },
    buttonText: {
        color: "white"
    },
    logo: {
        flex: 1,
        width: 300,
        height: 100,
        resizeMode: "contain"
    }
});

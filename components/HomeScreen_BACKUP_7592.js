import React, { Component } from "react";
import { connect } from "react-redux";

import {
    View,
    Text,
    Image,
    StyleSheet,
    Button,
    ActivityIndicator
} from "react-native";

import {
    setYear,
    setStudent,
    setGroup,
    fetchCategories
} from "../actions/events";

import { navigate } from "../navigation/actions";

import PickerList from "./common/PickerList";

class HomeScreen extends Component {
    static navigationOptions = {
        title: ""
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.onStudentChange = this.onStudentChange.bind(this);
        this.onGroupChange = this.onGroupChange.bind(this);
        this.onParentChange = this.onParentChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {}

    onParentChange(e) {
        if (e !== null) {
            this.props.dispatch(setYear(e));
        }
    }

    onGroupChange(e) {
        const { setYear } = this.props.credentials;

        const categories = this.props.categories;
        if (e !== null) {
            this.props.dispatch(setGroup(e, categories, setYear));
        }
    }

    onStudentChange(e) {
        if (e !== null) {
            this.props.dispatch(setStudent(e));
        }
    }

    onSubmit(e) {
        this.props.navigation.navigate("Month");
    }

    render() {
        const {
            setYear,
            setGroup,
            setStudent,
            showStudent
        } = this.props.credentials;
        const categories = this.props.categories;
        const isLoading = this.props.isLoading;

        if (isLoading || !categories) {
            return (
                <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size="large" color="#fff" />
                </View>
            );
        }

        return (
            <View style={styles.container}>
                <Image
                    source={require("../img/logoGreen.png")}
                    style={{ width: 300, height: 200 }}
                />

                <View style={{ backgroundColor: "red" }}>
                    <PickerList
                        selectedValue={setYear}
                        label="Year"
                        onChange={this.onParentChange}
                        options={categories}
                    />
                    {setYear
                        ? <PickerList
                              selectedValue={setGroup}
                              label="Year group"
                              onChange={this.onGroupChange}
                              options={
                                  categories.find(category => {
                                      return category.id == setYear;
                                  }).options
                              }
                          />
                        : null}
                    {setGroup && showStudent
                        ? <PickerList
                              selectedValue={setStudent}
                              label="Year group"
                              onChange={this.onStudentChange}
                              options={
                                  categories
                                      .find(category => {
                                          return category.id == setYear;
                                      })
                                      .options.find(category => {
                                          return category.id == setGroup;
                                      }).options
                              }
                          />
                        : null}
                    {setYear && setGroup && !showStudent || setStudent
                        ? <Button
                              onPress={this.onSubmit}
                              title="Continue"
                              color="#841584"
                          />
                        : null}
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        setParentCategory: state.setParentCategory,
        setSubCategory: state.setSubCategory,
        categories: state.fetchCategories,
        isLoading: state.isLoading,
        credentials: state.setCredentials
    };
};

export default connect(mapStateToProps)(HomeScreen);

const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
<<<<<<< HEAD
        backgroundColor: "#67999A",
        flex: 1
=======
        display: "flex",
        padding: 30
    },
    button: {
        alignItems: "center",
        backgroundColor: "#800080",
        padding: 10
    },
    picker: {
        width: 300,
        height: 30
    },
    buttonText: {
        color: "white"
>>>>>>> release/v1
    },
    logo: {
        flex: 1,
        width: 300,
        height: 100,
        resizeMode: "contain"
    }
});

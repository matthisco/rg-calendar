import { AsyncStorage } from "react-native";

import {
    isLoading,
    hasErrored,
    fetchSuccessCategories,
    fetchSuccessEvents,
    isLoadingCredentials,
    fetchErrorEvents
} from "./loader";

import { navigate } from "../navigation/actions";

export function fetchEvents(id) {
    const categoryId = encodeURIComponent(id);
    const url = `http://wordpress.rguc.co.uk/index.php/wp-json/tribe/events/v1/events?categories=${categoryId}`;

    return dispatch => {
        dispatch(isLoading(true));
        fetch(url)
            .then(response => {
                dispatch(isLoading(false));
                return response;
            })
            .then(response => response.json())
            .then(data => {
                const { events } = data;
                if (events) {
                    dispatch(fetchSuccessEvents(data));
                } else {
                    dispatch(fetchErrorEvents(data));
                }
            })
            .catch(error => {
                dispatch(hasErrored(error));
            });
    };
}

export const setCredentials = data => ({
    type: "SET_CREDENTIALS",
    data
});

export function getCredentials() {
    return dispatch => {
        AsyncStorage.getItem("loggedIn")
            .then(this.props.dispatch(isLoadingCredentials(true)))
            .then(
                data =>
                    data
                        ? this.props
                              .dispatch(setCredentials(JSON.parse(data)))
                              .then(this.props.dispatch(navigate("Month")))
                              .then(
                                  this.props.dispatch(
                                      isLoadingCredentials(false)
                                  )
                              )
                        : this.props.dispatch(isLoadingCredentials(false))
            );
    };
}

export const filterEvents = (data, id) => ({
    type: "FILTER_EVENTS",
    data,
    id
});

export const eventsForMonth = (data, day) => ({
    type: "EVENTS_MONTH",
    data,
    day
});

export const eventsForCalendarMonth = (data, day) => ({
    type: "EVENTS_CALENDAR_MONTH",
    data,
    day
});

export const setYear = int => ({
    type: "SET_YEAR",
    year: int
});

export const setGroup = (group, categories, year) => ({
    type: "SET_GROUP",
    group,
    categories,
    year
});

export const setStudent = int => ({
    type: "SET_STUDENT",
    student: int
});

export const clearCredentials = () => ({
    type: "CLEAR_CREDENTIALS"
});

export const resetData = () => ({
    type: "RESET_DATA"
});


export function resetForm() {
    return dispatch => {
        dispatch(clearCredentials());
        dispatch(navigate("Home"));
    };
}

function fetchPage(num) {
    const url = `http://wordpress.rguc.co.uk/index.php/wp-json/tribe/events/v1/categories?per_page=60&page=${num}`;
    return fetch(url)
        .then(response => {
            if (!response.ok) throw new Error(response.statusText);
            return response.json();
        })
        .catch(error => {
            dispatch(hasErrored(error));
        });
}

export function fetchCategories() {
    return dispatch => {
        dispatch(isLoading(true));
        Promise.all([fetchPage(1), fetchPage(2)]).then(
            data => {
                dispatch(isLoading(false));
                dispatch(
                    fetchSuccessCategories([
                        ...data[0].categories,
                        ...data[1].categories
                    ])
                );
            },
            err => {
                dispatch(isLoading(false));
                dispatch(hasErrored(true));
            }
        );
    };
}

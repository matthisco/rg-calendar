import React, { Component } from "react";
import { StyleSheet } from "react-native";

import { Provider } from "react-redux";
import { AppLoading, Asset, Font } from "expo";
import { FontAwesome } from "@expo/vector-icons";
import store from "./config/configureStore";

import AppNavigation from "./navigation";
import { fetchCategories } from "./actions/events";

// dispatch action to get array of events for app
store.dispatch(fetchCategories());

function cacheImages(images) {
	return images.map(image => {
		if (typeof image === "string") {
			return Image.prefetch(image);
		} else {
			return Asset.fromModule(image).downloadAsync();
		}
	});
}

function cacheFonts(fonts) {
	return fonts.map(font => Font.loadAsync(font));
}

export default class App extends React.Component {
	state = {
		isReady: false
	};

	async _loadAssetsAsync() {
		const imageAssets = cacheImages([
			require("./img/splash.png"),
			require("./img/CalendarApp_logo.png"),
			require("./img/RGUC_Connect_logo.png"),
			require("./img/bkgPhoto.jpg")
		]);

		const fontAssets = cacheFonts([FontAwesome.font]);

		await Promise.all([...imageAssets, ...fontAssets]);
	}

	render() {
		if (!this.state.isReady) {
			return (
				<AppLoading
					startAsync={this._loadAssetsAsync}
					onFinish={() => this.setState({ isReady: true })}
					onError={console.warn}
				/>
			);
		}

		return (
			<Provider store={store}>
				<AppNavigation />
			</Provider>
		);
	}
}

![GitHub Logo](/img/logo.png)

# Calendar App for the Ron Grimley Undergrauate Center

This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

![GitHub Logo](/walkthrough.gif)

## Install instructions

Npm install then npm start

## url https://exp-shell-app-assets.s3-us-west-1.amazonaws.com/ios%2F%40mharrisweb%2Frguc-calendar-41e6233a-7f5f-4af1-8874-6db648685b3a-archive.ipa
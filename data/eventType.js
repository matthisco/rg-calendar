export  const eventColour = {
	eventColour: [
		{
			type: "Lectures",
			colour: "#006666"
		},
		{
			type: "Bedside",
			colour: "#006699"
		},
		{
			type: "Workshops",
			colour: "#003366"
		},
		{
			type: "Simulation",
			colour: "#666699"
		},
		{
			type: "Clinics",
			colour: "#996699"
		},
		{
			type: "Ward based",
			colour: "#3399CC"
		},
		{
			type: "Clinical skills",
			colour: "#33CCCC"
		},
		{
			type: "Blocked Out",
			colour: "#999999"
		}
	]
};

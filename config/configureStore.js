import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";

import rootReducer from "../reducers";

const initialState = {
	hasErrored: false,
	isLoading: true,
	isLoadingCredentials: true,
	fetchEvents: {},
	fetchCategories: []
};

const reduxLogger = createLogger();

const store = createStore(
	rootReducer,
	initialState,
	applyMiddleware(thunk, reduxLogger)
);

export default store;

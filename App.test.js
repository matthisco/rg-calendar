import React from 'react';
import App from './App';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const rendered = renderer.create(<App />).toJSON();
  expect(rendered).toBeTruthy();
});

describe('App component renders the todo correctly', () => {
  it('renders correctly', () => {
    const rendered = renderer.create(
      <App />
    );
    expect(rendered.toJSON()).toMatchSnapshot();
  });
});

// test('TodoComponent renders the text inside it', () => {
//   const todo = { id: 1, done: false, name: 'Buy Milk' };
//   const wrapper = mount(
//     <Todo todo={todo} />
//   );
//   const p = wrapper.find('.toggle-todo');
//   expect(p.text()).toBe('Buy Milk');
// });
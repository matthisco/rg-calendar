import React, { Component } from "react";

import { connect } from "react-redux";
import {
  Image,
  View,
  Text,
  Modal,
  Button,
  TouchableOpacity,
  AsyncStorage,
  StyleSheet,
  Platform,
  Alert,
  TouchableHighlight
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { navigate } from "./actions";
import {
  setYear,
  setStudent,
  setGroup,
  fetchCategories,
  resetForm,
  resetData,
  fetchEvents
} from "../actions/events";

import Picker from "../components/common/PickerList";

class CustomHeader extends Component {
  constructor() {
    super();
    this.state = {
      year: false,
      group: false,
      student: false
    };

    this.resetForm = this.resetForm.bind(this);
    this.fetchEvents = this.fetchEvents.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }

  onGroupChange(e) {
    if (e !== "") {
      const { year } = this.props.credentials;
      const { setGroup } = this.props;
      const categories = this.props.categories;
      setGroup(e, categories, year);
    }
  }

  onYearChange(e) {
    const { navigate, setYear } = this.props;
    setYear(e);

    navigate("Month");
  }

  onStudentChange(e) {
    if (e !== "") {
      setStudent(e);
    }
  }

  resetForm() {
    const { resetForm } = this.props;

    AsyncStorage.removeItem("loggedIn", resetForm());
  }

   showAlert(){
      Alert.alert(
         'Events refreshed'
      )
   }

  fetchEvents() {
    const {
      fetchEvents,
      navigate,
      credentials: { group }
    } = this.props;
    resetData();
    fetchEvents(group);
    navigate("Month");
    this.showAlert();
  }

  render() {
    const categories = this.props.categories;
    const { year, group, student, showStudent } = this.props.credentials;
    const { setYear, setGroup, setStudent } = this.props;
    const buttonOptions = [
      {
        key: 0,
        label: "refresh",
        value: 1,
        icon: "undo"
      },
      {
        key: 1,
        label: "back",
        value: 2,
        icon: "home"
      },
      {
        key: 2,
        label: "back",
        value: 3,
        icon: "calendar"
      }
    ];

    return (
      <View style={styles.container}>
        <View style={styles.containerTop}>
          <View style={styles.divContainerTopLeft} key={"view1"}>
            <TouchableOpacity onPress={this.resetForm}>
              <Icon
                name="home"
                style={styles.button}
                color="white"
                key={"icon0"}
                size={40}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.divContainerTopRight} key={"view2"}>
            <TouchableOpacity onPress={this.fetchEvents}>
              <Icon
                name="refresh"
                style={styles.button}
                color="white"
                key={"icon2"}
                size={40}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    resetForm: () => dispatch(resetForm()),
    fetchEvents: id => dispatch(fetchEvents(id)),
    navigate: route => dispatch(navigate(route)),
    resetData: () => dispatch(resetData())
  };
};

const mapStateToProps = state => {
  return {
    categories: state.fetchCategories,
    isLoading: state.isLoading,
    credentials: state.setCredentials
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomHeader);


const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFFFFF"
  },
  containerTop: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: "#006666",
    height: 50,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10
  },
  containerBottom: {
    height: 30,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: "row"
  },
  divContainerTopLeft: {
    borderBottomLeftRadius: 10,
    flex: 6,
    paddingLeft: 8,
    paddingTop: 5
  },
  divContainerTopRight: {
    borderRadius: 0,
    marginRight: 8,
    marginBottom: 5,
    flexDirection: "row",
    alignItems: "flex-end",
    borderBottomRightRadius: 10
  },
  button: {
    borderRadius: 0,
    color: "#fff"
  },
  buttonInvert: {
    borderRadius: 0,
    backgroundColor: "#fff",
    color: "#67999A",
    margin: 5
  }
});

import { NavigationActions } from 'react-navigation';

export const navigate = (routeName, params, action) =>
  NavigationActions.navigate({ routeName, params, action });

export const goBack = sceneKey => NavigationActions.back({ key: sceneKey });

export const setParams = (params, key) =>
  NavigationActions.setParams({ params, key });






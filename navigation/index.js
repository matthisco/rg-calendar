import React, { Component } from "react";
import { connect } from "react-redux";
import {
  BackHandler,
  View,
  StatusBar,
  Platform,
  StyleSheet
} from "react-native";
import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware
} from "react-navigation-redux-helpers";
import { addNavigationHelpers } from "react-navigation";
import NavigationStack from "./navigationStack";

const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav
);
const addListener = createReduxBoundAddListener("root");

class AppNavigation extends Component {
  componentWillMount() {
    if (Platform.OS !== "android") return;
    BackHandler.addEventListener("hardwareBackPress", () => {
      const { navigationState, dispatch } = this.props;
      const { index } = navigationState;
      if (
        navigationState.routes[index].routeName === "Month" ||
        navigationState.routes[index].routeName === "Day"
      ) {
        dispatch({ type: "Navigation/BACK" });
        return true;
      }
      BackHandler.exitApp();
      return false;
    });
  }

  componentWillUnmount() {
    if (Platform.OS === "android")
      BackHandler.removeEventListener("hardwareBackPress");
  }

  render() {
    const { navigationState, dispatch } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar translucent backgroundColor="#000" />
        <View style={styles.appBar} />
        <View style={styles.content}>
          <NavigationStack
            navigation={addNavigationHelpers({
              dispatch,
              state: navigationState,
              addListener
            })}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    navigationState: state.navigationReducer
  };
};

export default connect(mapStateToProps)(AppNavigation);

const STATUSBAR_HEIGHT = Platform.OS === "ios" ? 20 : StatusBar.currentHeight + 1;
const APPBAR_HEIGHT = Platform.OS === "ios" ? 23 : 23;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    height: STATUSBAR_HEIGHT
  },
  appBar: {
    backgroundColor: "#79B45D",
    height: APPBAR_HEIGHT
  },
  content: {
    flex: 1,
    backgroundColor: "#33373B"
  }
});

import { StackNavigator } from "react-navigation";
import HomeScreen from "../components/HomeScreen";
import Day from "../components/calendar/Agenda";
import Month from "../components/calendar/Month";

const AppNavigator = StackNavigator({
	Home: {
		screen: HomeScreen,
		navigationOptions: ({ navigation }) => ({
			header: false
		})
	},
	Month: {
		screen: Month
	},
	Day: {
		screen: Day
	}
});

export default AppNavigator;

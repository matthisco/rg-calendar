import { combineReducers } from "redux";
import {
	setCredentials,
	fetchCategories,
	eventsForMonth,
	fetchEvents
} from "./events";

import { hasErrored, isLoading, isLoadingCredentials } from "./loader";

import navigationReducer from "./navigationReducer";
const rootReducer = combineReducers({
	fetchEvents,
	navigationReducer,
	hasErrored,
	eventsForMonth,
	isLoading,
	isLoadingCredentials,
	fetchCategories,
	setCredentials
});

export default rootReducer;

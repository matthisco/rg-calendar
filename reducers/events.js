import * as helpers from "../helpers";

import { eventColour } from "../data/eventType.js";

export function filterEvents(state = [], action) {
    switch (action.type) {
        case "FILTER_EVENTS":
            const events = action.data;
            const selectedId = action.id;

            const results = Object.values(events).filter(o =>
                o.dots.some(oo => oo.categories.some(c => c.id === selectedId))
            );

            return {
                results
            };

        default:
            return state;
    }
}

export function fetchEvents(state = {}, action) {
    switch (action.type) {
        case "EVENTS_SUCCESS":
            const events = {};

            const filteredEvents = action.data.events.filter(
                event => event.start_date >= helpers.currentDate
            );

            filteredEvents.map(event => {
                const {
                    year: startYear,
                    month: startMonth,
                    day: startDay,
                    hour: startHour,
                    minutes: startMinute
                } = event.start_date_details;

                const {
                    hour: endHour,
                    minutes: endMinute
                } = event.end_date_details;

                const {
                    description,
                    categories,
                    id,
                    allDay: all_day,
                    title,
                    venue: { venue }
                } = event;

                const organiser =
                    event.organizer.length != 0
                        ? event.organizer[0].organizer
                        : "N/A";

                const colourMatch =
                    event.tags.length > 0
                        ? eventColour.eventColour.find(
                              obj => obj.type === event.tags[0].name
                          )
                        : "";
                const colour = colourMatch && colourMatch.colour;
                const textColor = colour ? "#ffffff" : "#000000";
                const startDate = `${startYear}-${startMonth}-${startDay}`;
                const startTime = `${startHour}-${startMinute}`;
                const endTime = `${endHour}-${endMinute}`;

                let newEvent = {
                    id,
                    title,
                    colour,
                    textColor,
                    organiser,
                    description,
                    venue,
                    selectedDotColor: colour,
                    categories: event.categories,
                    startTime,
                    endTime
                };

                events[startDate] = events[startDate] || { dots: [] };

                const dotsEvents = [...events[startDate].dots, newEvent];

                dotsEvents.sort((a, b) => a.startTime - b.startTime);

                events[startDate] = {
                    dots: dotsEvents,
                    disabled: false,
                    selected: true,
                    selectedColor: "#00CCCB",
                    customStyles: {
                        text: {
                            marginTop: 3
                        }
                    }
                };
            });

            return {
                ...events
            };
        case "EVENTS_FETCH_ERROR":
            return state;
        case "REFRESH_DATA":
            return { state, fetchEvents: {} };
        case "CLEAR_CREDENTIALS":
            return {};
        default:
            return state;
    }
}

function timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split("T")[0];
}

export function eventsForMonth(state = {}, action) {
    switch (action.type) {
        case "EVENTS_MONTH": {
            const { data: events, day } = action;
            const newItems = {};

            for (let i = -15; i < 85; i++) {
                const time = day.timestamp + i * 24 * 60 * 60 * 1000;
                const strTime = timeToString(time);

                if (events[strTime]) {
                    newItems[strTime] = [...events[strTime].dots];
                } else {
                    newItems[strTime] = [];
                }
            }

            return {
                ...newItems
            };
        }
        case "EVENTS_CALENDAR_MONTH": {
            const { data, day } = action;
            const newItems = {};

            for (let i = -15; i < 85; i++) {
                const time = day[0].timestamp + i * 24 * 60 * 60 * 1000;

                const strTime = timeToString(time);

                if (data[strTime]) {
                    newItems[strTime] = { ...data[strTime] };
                } else {
                    newItems[strTime] = {};
                }
            }
            return {
                ...newItems
            };
        }
        default:
            return state;
    }
}

function sortName(a, b) {
    const textA =
        a.name.replace(/\D/g, "") != ""
            ? Number(a.name.replace(/\D/g, ""))
            : a.name.toUpperCase();

    const textB =
        b.name.replace(/\D/g, "") != ""
            ? Number(b.name.replace(/\D/g, ""))
            : b.name.toUpperCase();
    return textA < textB ? -1 : textA > textB ? 1 : 0;
}

function hasNumber(myString) {
    return /\d/.test(myString);
}

export function fetchCategories(state = [], action) {
    switch (action.type) {
        case "CATEGORY_SUCCESS":
            const yearCategories = action.data.filter(
                category => category.parent == 0
            );
            const categorys = yearCategories.map(
                ({ id, name: label, parent }) => ({
                    id,
                    label,
                    parent,
                    options: action.data
                        .filter(({ parent }) => parent === id)
                        .sort(sortName)
                        .map(({ id, name: label, parent }) => ({
                            id,
                            parent,
                            label,
                            options: action.data
                                .filter(({ parent }) => parent === id)
                                .sort(sortName)
                                .map(({ id, name: label, parent }) => ({
                                    id,
                                    label,
                                    parent
                                }))
                        }))
                })
            );
            return [...categorys];

        default:
            return state;
    }
}

function checkEmptyArray(args) {
    const { year, group, categories } = args;
    return (
        categories
            .find(category => {
                return category.id == year;
            })
            .options.find(category => {
                return category.id == group;
            }).options.length == 0
    );
}

const initialState = {
    showStudent: false
};
export function setCredentials(state = initialState, action) {
    switch (action.type) {
        case "SET_YEAR":
            return {
                ...state,
                year: action.year,
                student: 0
            };
        case "SET_STUDENT":
            return { ...state, student: action.student };
        case "SET_GROUP":
            const showStudent = !checkEmptyArray(action);
            return { ...state, group: action.group, showStudent };
        case "SET_CREDENTIALS":
            const { data } = action;
            return { ...state, ...data };
        case "CLEAR_CREDENTIALS":
            return initialState;

        default:
            return state;
    }
}
